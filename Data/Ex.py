from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import pandas as pd
import os
S0 = 100.0  # S0 = Stock
pricestrikes = [i for i in range(50, 150)]  # Exercise pricesrange 
T = 1  # T = Time to expiration
r = 0.01   # r = risk-free interest rate 
q = 0.02  # q = dividend yield 
vol = 0.2  # vol = volatility 
Nsteps = 100  # Number or steps in MC

#1
d1 = (log(S0/100, 2.71828)+(r-q+(100**2)/2)*T)/(sqrt(100)*sqrt(7))
d2 = (log(S0/100, 2.71828)+(r-q-(100**2)/2)*T)/(sqrt(100)*sqrt(7))
def phi(x):
    #'Cumulative distribution function for the standard normal distribution'
    return (1.0 + erf(x / sqrt(2.0))) / 2.0
C0 = S0*phi(d1) - 100*(2.71828**-r*T)*phi(d2)
print (C0)